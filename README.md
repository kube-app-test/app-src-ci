# Kubernetes Stack - test app

#### CI Pipeline
Gitlab CI should build and push the docker image to Docker Hub using pipeline variables when new commits are pushed, then trigger a deploy job.

###### CI variables needed
```sh
$DOCKER_USERNAME > docker hub login name
$DOCKER_PASSWORD > docker hub token
$FRUITS_IMAGE_NAME > image
$VEG_IMAGE_NAME > image
$PRODUCTS_IMAGE_NAME > image
$REDISAPI_IMAGE_NAME > image
$BUILDX_VERSION > vX.Y.Z
```
###### Architecture comptability
```sh
Plugin: docker buildx v0.5.1
Architectures: AMD64 and ARM64(aarch64-v8).
```
###### Must watch pages
```sh
https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html
https://docs.gitlab.com/ee/ci/multi_project_pipelines.html

Runners configs and CI container pull policy
https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-docker-section
https://docs.gitlab.com/runner/executors/docker.html#how-pull-policies-work
```
###### Syntax notes
**https://docs.gitlab.com/ee/ci/yaml/**

###### Variable ref
**https://docs.gitlab.com/ee/ci/variables/predefined_variables.html**

###### Docker build example
**https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/**
