const app = require('express')();
const bodyParser = require('body-parser');


// Importing async calls
const { get, set } = require('./async-store.js');

// JSON parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const port = process.env.PORT || 3000;



// get from redis.
// /:key are called MIDDLEWARE function's path
// /Orange should return the Value of the KV
app.get('/:key', async (req, res) => {
    const message = await get(req.params.key).catch((err) => {
        if (err) console.error(err)
    });

    // send response
    res.send({
        status: 200,
        message: message
    })
});

// write to redis
// A post request to /send endpoint with "key" and "value" provided as JSON
// should save that KV on redis
app.post('/send', async (req, res) => {
    await set(req.body.key, req.body.value);

    // send response
    res.send({
        status: 200,
        message: 'Data has been stored in redis'
    })
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
  });