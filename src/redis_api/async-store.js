const redis = require('redis');
const { promisify } = require("util");
// https://docs.redislabs.com/latest/rs/references/client_references/client_nodejs/


const redis_password = process.env.REDIS_PWD || "guest";
const redis_host = process.env.REDIS_HOST || "localhost";
const redis_port = process.env.REDIS_PORT || 6379;


const client = redis.createClient({
    host: `${redis_host}`,
    port: `${redis_port}`,
    password: `${redis_password}`
});

client.on('connect', () => {
    console.log('Redis client connected');
});

client.on("error", (error) => {
    console.error(error);
    process.exit(1);
});

const get = promisify(client.get).bind(client);
const set = promisify(client.set).bind(client);
const getList = promisify(client.lrange).bind(client);

module.exports = {
    get,
    set,
    getList
};