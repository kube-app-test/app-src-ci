var express = require("express");
var app = express();

var amqp = require('amqplib');
// Random comment

const rabbit_user = process.env.RABBITMQ_USER || "guest";
const rabbit_pwd = process.env.RABBITMQ_PWD || "guest";
const rabbit_host = process.env.RABBITMQ_HOST || "localhost";
const rabbit_port = process.env.RABBITMQ_PORT || 5672;
// const vhost = process.env.RABBIT_VHOST;.

const rabbit_con_string = `amqp://${rabbit_user}:${rabbit_pwd}@${rabbit_host}:${rabbit_port}`


const port = process.env.PORT || 3000;
const fruit_host = process.env.FRUIT_HOST || "localhost";
const fruit_port = process.env.FRUIT_PORT || 3000;
const vegetables_host = process.env.VEGETABLES_HOST || "localhost";
const vegetables_port = process.env.VEGETABLES_PORT || 3000;

const urlFruit = `http://${fruit_host}:${fruit_port}`
const urlVeg = `http://${vegetables_host}:${vegetables_port}`

var jsonResponse;

app.get('/fruits', (_, res) => {

  async function fruitClient(){
    try {
      let conn = await amqp.connect(rabbit_con_string);
      let ch = await conn.createChannel();
      let q = await ch.assertQueue('',{
        exclusive: true
      })

      var correlationId = generateUuid();
      var requestedPriceCharge = parseFloat(Math.random()).toFixed(2)
      console.log(' [x] Requesting fruits prices to charge extra by %d', requestedPriceCharge);

      await ch.consume(q.queue, msg => {
        if (msg.properties.correlationId == correlationId) {
          jsonResponse = JSON.parse(msg.content);

          res.setHeader('Content-Type', 'application/json');
          res.send(jsonResponse)

          setTimeout(function() { 
            conn.close(); 
            // process.exit(0) 
          }, 500);
        }
      }, {
        noAck: true
      });

      ch.sendToQueue('rpc_queue_FRUITS',
        Buffer.from(requestedPriceCharge.toString()),{ 
          correlationId: correlationId, 
          replyTo: q.queue });
    } catch (err){
      console.error(err);
    }
  }

  fruitClient();
  
});


app.get('/vegetables', (_, res) => {

  async function vegetableClient(){
    try {
      let conn = await amqp.connect(rabbit_con_string);
      let ch = await conn.createChannel();
      let q = await ch.assertQueue('',{
        exclusive: true
      })

      var correlationId = generateUuid();
      var requestedPriceCharge = parseFloat(Math.random()).toFixed(2)
      console.log(' [x] Requesting vegetables prices to charge extra by %d', requestedPriceCharge);

      await ch.consume(q.queue, msg => {
        if (msg.properties.correlationId == correlationId) {
          jsonResponse = JSON.parse(msg.content);

          res.setHeader('Content-Type', 'application/json');
          res.send(jsonResponse)

          setTimeout(function() { 
            conn.close(); 
            // process.exit(0) 
          }, 500);
        }
      }, {
        noAck: true
      });

      ch.sendToQueue('rpc_queue_VEGETABLES',
        Buffer.from(requestedPriceCharge.toString()),{ 
          correlationId: correlationId, 
          replyTo: q.queue });
    } catch (err){
      console.error(err);
    }
  }

  vegetableClient();
});




app.listen(port, () => {
  console.log(`Server running on port ${port}`);
  console.log(`Vegetables API endpoint: ${urlVeg} \nFruits API endpoint: ${urlFruit}`);
});


function generateUuid() {
  return Math.random().toString() +
         Math.random().toString() +
         Math.random().toString();
}
