var express = require("express");
var app = express();
const fs = require('fs');
var amqp = require('amqplib');
const axios = require('axios');


const rabbit_user = process.env.RABBITMQ_USER || "guest";
const rabbit_pwd = process.env.RABBITMQ_PWD || "guest";
const rabbit_host = process.env.RABBITMQ_HOST || "localhost";
const rabbit_port = process.env.RABBITMQ_PORT || 5672;
// const vhost = process.env.RABBIT_VHOST;

const rabbit_con_string = `amqp://${rabbit_user}:${rabbit_pwd}@${rabbit_host}:${rabbit_port}`


// Internal ports used for services inside the cluster.
const port = process.env.PORT || 3000;


// Service name of redis custom async api.
const redis_api = process.env.REDIS_API || "redis-api";

const redis_api_uri = `${redis_api}:${port}`

// Response sent back through MQ
var jsonResponse = { products: [] };


// Reading data file from FS (could be replaced by database).
let rawdata = fs.readFileSync('data/fruits.json');
let fruits = JSON.parse(rawdata).products;
let fruitsLoop = fruits;





async function server(){
  try {
      let conn = await amqp.connect(rabbit_con_string);
      let ch = await conn.createChannel();
      let q = await ch.assertQueue('rpc_queue_FRUITS', {
        durable: false
      });

      ch.prefetch(1);
      console.log(' [x] Awaiting RPC requests');


      // Try await ch.consume(q.queue,reply(msg)=>{
      await ch.consume(q.queue,msg=>{

        // Consuming message from queue.
        var requestedPriceCharge = msg.content.toString();

        console.log(" [.] Applying extra charge: (%d)", requestedPriceCharge);


        function calculateNewPrices() {
          return new Promise(resolve => {

            async function queryRedis() {
      
              let res = await axios.get(`http://${redis_api_uri}/Fruits-${requestedPriceCharge}`);


              // If the data doesn't exist in REDIS, calculate it.

              if (res.data.message == null) {

                console.log('Calculating new products prices, this may take a few seconds...')

                // Here I'm simulating a high calculation time for the prices, so calculating it and pulling it from REDIS would give much obvious results.
                setTimeout(() => {
                  // Looping through list of fruits and modifying price field
                  fruitsLoop.forEach( async fruit => {

                  // Calculating the new price of products
                  var newPrice = parseFloat((parseFloat(fruit.price) * (1 + parseFloat(requestedPriceCharge))).toFixed(2))
                  // console.log(`"id": ${fruit.id}, "type": "${fruit.type}", "name": "${fruit.name}", "price": "${newPrice}"`)


                  // Creating the array that will be stored on REDIS and returned through API to client
                  jsonResponse.products.push({id: fruit.id, type: fruit.type, name: fruit.name, price: newPrice});
                });


                // Forging POST request to store list of prices for a particular charge rate on REDIS.
                async function storeInRedis() {

                  params = {
                      key: `Fruits-${requestedPriceCharge}`,
                      value: JSON.stringify(jsonResponse)  // This had a stringify + replace
                    }
          
                  let res = await axios.post(`http://${redis_api_uri}/send`, params);
          
                  console.log(res.data);
                }
                storeInRedis()

                // Resolving promise
                resolve(jsonResponse)
                }, 4000);

              } else {

                console.log('Pulling new prices from REDIS Cache.')

                // If the data exists, just resolve the promise with it. (REMINDER: I'm stringifying the data before sending it to REDIS)

                jsonResponse = res.data.message
                resolve(jsonResponse)
              }
            }
            queryRedis()
            
          })
        }


        // I'm simulating that calculating new prices have a computational cost (of 5 seconds), that could be avoided using REDIS but the data must previously exist.
        // jsonResponse is generated, returned as a Promise and captured on a new variable called response.
        async function sendToQueue() {
          const response = await calculateNewPrices();

          ch.sendToQueue(msg.properties.replyTo,
          Buffer.from(JSON.stringify(response)), {
            correlationId: msg.properties.correlationId
          });
          ch.ack(msg);
  
          // Cleaning up modified JSON variable
          jsonResponse = { products: [] };
          fruitsLoop = fruits;
        }

        sendToQueue();

      });

  } catch(err) {
      console.error(err);
      process.exit(1);
  }
}

server();



app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
